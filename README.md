# Projeto_33_entrega

Projeto com a organização pretendida para a entrega da UC Projeto final de curso

Guia de instalação da aplicação localmente.

Tecnologias necessárias para o bom funcionamento da aplicação:

- Node.js - versão 14.16.0 - https://nodejs.org/dist/v14.16.0/

- OpenJDK11 ou superior - https://openjdk.java.net/

- PostgreSQL versão 13 - https://www.postgresql.org/download/

Depois da instalação das tecnologias instaladas. 

passo 1 - clonar/fazer download o repositório  https://gitlab.com/projectoDanielRui/multimedia

passo 2 - colocar pastas existentes na pasta museu\03_Implementacao\src\main\webapp\content

passo 3 - criar uma base de dados com o PgAdmin chamada "museu".
    username: postgres
    password: root

passo 4 - importar o projeto no eclipse através da funcionalidade "Import maven projects"

passo 5 - executar o projeto, este deverá correr frontend e backend juntos assim como preencher base de dados com os recursos necessários.
devido a instalar node modules, garanta que tem conexão a internet para este passo. Usar JDK11.

passo 6 - o último print na consola deverá mostrar os enderecos onde se encontra a aplicação. Quando isto acontecer poderá abrir a aplicação em localhost:8080

existe já um utilizador teste em que as credenciais são
    username = user
    password = user


